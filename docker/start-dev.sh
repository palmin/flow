#!/bin/bash

# Collect static
python manage.py collectstatic --noinput

# Run migrations
python manage.py migrate

# Start Gunicorn processes
echo Starting dev server
python manage.py runserver 0.0.0.0:8000