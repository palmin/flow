# Flow

Flow is a form builder and case management software suitable for managing cases within an organisation. It allows you to easily build responsive forms and track
cases and link external API's.

## Development

Copy `.env-exmple` en `.env` en update credentials

Start development server:

docker-compose up

### Create superuser

Execute this command to create an admin in Django:

```shell
docker-compose exec flow python manage.py createsuperuser
```

Visit http://localhost:8001/admin and file a request for access.

Next, open a python shell

```shell
docker-compose exec flow python manage.py shell
```

Next, copy information from the access request to the superuser.

```python
from pleio_auth.models import AccessRequest, User

for record in AccessRequest.objects.all():
    admin = User.objects.filter(email=record.email).first()
    if admin:
        admin.external_id = record.external_id
        admin.save()
        record.delete()
```

Next visit http://localhost:8001/admin again.

You are in...

### Provisioning

```python
from core.factories import CaseFactory, CaseTypeFactory, CaseTypePhaseFactory

casetype = CaseTypeFactory(name="Vraag", type='message')
CaseTypePhaseFactory(casetype=casetype, name="Intake")
CaseTypePhaseFactory(casetype=casetype, name="In progress")
CaseTypePhaseFactory(casetype=casetype, name="Done")

for n in range(0, 10):
    CaseFactory(casetype__name='Vraag')

```

## Backup and restore

### Backup

```shell
# All
python manage.py create_backup

# one case-type. usefull for migrating cases of one kind to another workflow instance
python manage.py create_backup --casetype [casetype]

# exclude files
python manage.py create_backup --exclude-files

# Create zip archive
python manage.py create_backup --compress

# If a date-stamp does not satisfy your needs, it is possible to provide a suggestion for a name.
# If another file or folder with that name already exists, an incremental integer value will be
# appended to the name.
python manage.py create_backup --name [alternative backup name]
```

### Restore

```shell

# restore a backup from a backup-name (not a path)
python manage.py restore_backup [name]

# Specify an alternative path to look for the backup.
python manage.py restore_backup [name] --root [path to folder where the backup can be found]
```

## Migrate cases
Move cases of one casetype to another flow instance:

* At the source flow instance:
    * Rename the casetype to a name that does not exist in the target system
    * Export the cases with a the `--casetype` option
* At the target flow instance:
    * Import the dump in the target system