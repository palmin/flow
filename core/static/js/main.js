$(function () {
    tinymce.init({
        selector: 'textarea:not(.mceNoEditor)',
        content_css: '/static/css/editor.css',
        browser_spellcheck: true,
        plugins: 'link,lists,code,fullscreen',
        language: 'nl',
        relative_urls: false,
        remove_script_host: true,
        toolbar: 'bold italic underline | quicklink link numlist bullist alignleft aligncenter alignright alignjustify | code fullscreen',
        menubar: false,
        statusbar: false
    });

    window.addEventListener("beforeunload", function (e) {
        var confirmationMessage = "Sommige wijzigingen zijn niet opgeslagen. Weet je zeker dat je de pagina wil verlaten?";

        if (tinyMCE.activeEditor && tinyMCE.activeEditor.isDirty()) {
            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage;
        }
    });

    $('#id_closing_tasks').hide();
    $('#id_phase').change(function (e) {
        // check if last option is selected
        if (e.target.selectedIndex === (e.target.length - 1)) {
            $('#id_closing_tasks').show();
        } else {
            $('#id_closing_tasks').hide();
        }
    })

    $('#comment_filter_form select').change(function (e) {
        $(this).parent().submit();
    })
});