from importlib import import_module
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings


class CloseTaskBase:
    label = None

    @classmethod
    def on_close(self, case, comment, request):
        pass


class BaseCaseTypeWrapper:
    closing_tasks = []

    def handle_autoresponder(self, case):
        pass

class CaseTypeManager:
    def __init__(self):
        self.casetypes = {k: self.initialize_case_type(k, v) for k, v in settings.CASE_TYPES.items()}

    def initialize_case_type(self, key, settings):
        try:
            backend = import_module('%s.base' % settings['ENGINE']).CaseTypeWrapper
            return backend(key)
        except ImportError as e_user:
            raise ImproperlyConfigured(
                "%s is not a valid case type.\n"
                "Please check core.casetypes.XXX and verify the name." % (settings['ENGINE'])
            ) from e_user

    def options(self):
        return [(k, k) for k in self.casetypes.keys()]
