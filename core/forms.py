from django.forms import (ModelForm, Form, CharField, MultipleChoiceField,
                          ModelChoiceField, ModelMultipleChoiceField, ChoiceField,
                          FileField, ClearableFileInput, Textarea, DateField,DateInput,
                          CheckboxSelectMultiple, TextInput, Select)
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.forms import SimpleArrayField
from django.utils import timezone

from .models import Case, CaseType, CaseTypePhase, Comment, Setting
from pleio_auth.models import User
import bleach

class MultipleArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": MultipleChoiceField,
            "choices": self.base_field.choices,
            "widget": CheckboxSelectMultiple,
            **kwargs
        }
        return super(ArrayField, self).formfield(**defaults)

class FilterForm(Form):
    q = CharField(max_length=100, required=False, label='Zoekterm',
                  widget=TextInput(attrs={'placeholder': 'Zoekterm...', 'class': 'form-control'}))
    status = ChoiceField(choices=((None, '---------'), ('OPEN', 'Open'),
                                  ('CLOSED', 'Gesloten'), ('PENDING', 'In afwachting')),
                         required=False, label='Status', widget=Select(attrs={'class': 'form-control'}))
    casetype = ModelChoiceField(queryset=CaseType.objects, required=False, label='Type',
                                widget=Select(attrs={'class': 'form-control'}))
    phase = ModelChoiceField(queryset=CaseTypePhase.objects, required=False, label='Fase',
                             widget=Select(attrs={'class': 'form-control'}))
    manager = ModelChoiceField(queryset=User.objects, required=False, label='Behandelaar',
                               widget=Select(attrs={'class': 'form-control'}))
    responsible = ModelChoiceField(queryset=User.objects, required=False, label='Verantwoordelijke',
                                   widget=Select(attrs={'class': 'form-control'}))
    tags = SimpleArrayField(CharField(max_length=100), required=False, label='Tags',
                            widget=TextInput(attrs={'class': 'form-control'}))
    created_on_start = DateField(required=False, label='Startdatum',
                                   widget=DateInput(format='%d/%m/%Y'),input_formats=['%d/%m/%Y'])
    created_on_end = DateField(required=False, label='Startdatum',
                                   widget=DateInput(format='%d/%m/%Y'),input_formats=['%d/%m/%Y'])
    deadline_start = DateField(required=False, label='Deadline',
                                   widget=DateInput(format='%d/%m/%Y'),input_formats=['%d/%m/%Y'])
    deadline_end = DateField(required=False, label='Deadline',
                                   widget=DateInput(format='%d/%m/%Y'),input_formats=['%d/%m/%Y'])
    sort = CharField(max_length=100, required=False, label='',
                  widget=TextInput(attrs={'class': 'form-control'}))
    dir = CharField(max_length=100, required=False, label='',
                  widget=TextInput(attrs={'class': 'form-control'}))
    show = CharField(max_length=100, required=False, label='',
                  widget=TextInput(attrs={'class': 'form-control'}))

class SortingForm(Form):
    sort = CharField(max_length=100, required=False, label='',
                  widget=TextInput(attrs={'placeholder': 'Zoekterm...', 'class': 'form-control'}))
    dir = ChoiceField(choices=((None, '---------'), ('asc', 'Ascending'),
                                  ('desc', 'Descending')),
                         required=False, label='', widget=Select(attrs={'class': 'form-control'}))

class CommentFilterForm(Form):
    comment_type = ChoiceField(
        choices=(('internal', 'Interne reacties'), ('external', 'Externe reacties'), ('all', 'Alle reacties')),
        required=False, label='Filter reacties', widget=Select(attrs={'class': 'form-control'}))

class SettingForm(ModelForm):
    COLUMN_NAMES = (
        ("id", "ID"),
        ("name", "Naam"),
        ("status", "Status"),
        ("created_on", "Startdatum"),
        ("latest_phase_change", "Looptijd"),
        ("current_phase", "Fase"),
        ("deadline", "Deadline"),
        ("responsible", "Verantwoordelijke"),
        ("tags", "Tags")
    )
    shown_columns = MultipleArrayField(
        ChoiceField(
            choices=COLUMN_NAMES,
            required=False,label='Zichtbare kolommen', widget=Select(attrs={'class': 'form-control'})
        ),
        blank=True,
        null=True
    )
    
class CaseForm(ModelForm):
    attachments = FileField(widget=ClearableFileInput(attrs={'multiple': True, 'class': 'form-control'}),
                            required=False, label='Voeg bijlage(n) toe (optioneel)')

    def clean_description(self):
        description = self.cleaned_data.get('description', '')
        return bleach.clean(description, tags=settings.BLEACH_ALLOWED_TAGS,
                            attributes=settings.BLEACH_ALLOWED_ATTRIBUTES)

    def clean_deadline(self):
        deadline = self.cleaned_data.get('deadline', '')
        if deadline:
            return str(timezone.localtime(deadline))
        return None

    class Meta:
        model = Case
        fields = ['casetype', 'name', 'description', 'deadline', 'tags']
        labels = {
            'casetype': 'Type',
            'name': 'Naam',
            'description': 'Omschrijving'
        }

        widgets = {
            'casetype': Select(attrs={'class': 'form-control'}),
            'name': TextInput(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'class': 'form-control'}),
            'deadline': TextInput(attrs={'class': 'form-control'}),
            'tags': TextInput(attrs={'class': 'form-control'}),
        }


class CommentForm(ModelForm):
    attachments = FileField(widget=ClearableFileInput(attrs={'multiple': True, 'class': 'form-control'}),
                            required=False, label='Voeg bijlage(n) toe (optioneel)')

    def clean_description(self):
        description = self.cleaned_data.get('description', '')
        return bleach.clean(description, tags=settings.BLEACH_ALLOWED_TAGS,
                            attributes=settings.BLEACH_ALLOWED_ATTRIBUTES)

    class Meta:
        model = Comment
        fields = ['description']
        labels = {
            'description': ''
        }

        widgets = {
            'description': Textarea()
        }


class PhaseForm(Form):
    responsible = ModelChoiceField(queryset=User.objects.order_by('name'), required=False,
                                   label='Stel verantwoordelijke in op (optioneel)',
                                   widget=Select(attrs={'class': 'form-control'}))
    phase = ModelChoiceField(queryset=CaseTypePhase.objects, required=False, label='En verander fase naar (optioneel)',
                             widget=Select(attrs={'class': 'form-control'}))
    closing_tasks = MultipleChoiceField(widget=CheckboxSelectMultiple, label='Afrondende acties', required=False)

    def __init__(self, casetype=None, closing_tasks=[], *args, **kwargs):
        super(PhaseForm, self).__init__(*args, **kwargs)

        if casetype:
            self.fields['phase'].queryset = CaseTypePhase.objects.filter(casetype=casetype)

        if closing_tasks:
            self.fields['closing_tasks'].choices = closing_tasks


class NotificationsForm(Form):
    def __init__(self, *args, user=None, **kwargs):
        super(NotificationsForm, self).__init__(*args, **kwargs)

        all_subscriptions = [subscription.phase.id for subscription in user.subscriptions.all()]

        for casetype in CaseType.objects.all():
            self.fields[str(casetype.id)] = ModelMultipleChoiceField(
                queryset=casetype.phases,
                widget=CheckboxSelectMultiple,
                required=False,
                label=casetype.name,
                initial=all_subscriptions
            )
