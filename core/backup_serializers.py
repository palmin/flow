from backup.serializer import BackupSerializerBase
from core.models import Case, CaseLog, Comment, ExternalComment, Attachment


class CaseBackupSerializer(BackupSerializerBase):
    model = Case


class CaseLogBackupSerializer(BackupSerializerBase):
    model = CaseLog


class CommentBackupSerializer(BackupSerializerBase):
    model = Comment


class ExternalCommentBackupSerializer(BackupSerializerBase):
    model = ExternalComment


class AttachmentBackupSerializer(BackupSerializerBase):
    model = Attachment
