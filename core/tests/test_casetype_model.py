import datetime

from django.test import TestCase
from django.utils.timezone import localtime

from core.models import CaseType


class CaseTypeModelTestCase(TestCase):

    def test_model_fields(self):
        expected_result = dict(
            name="Expected name",
            type="simple",
            settings="{}",
            autoresponder_start=None,
            autoresponder_due=None,
            autoresponder_message=None,
        )

        case_type = CaseType.objects.create(**expected_result)

        self.assertEqual(case_type.name, expected_result.get('name'))
        self.assertEqual(case_type.type, expected_result.get('type'))
        self.assertEqual(case_type.settings, expected_result.get('settings'))
        self.assertEqual(case_type.autoresponder_start, expected_result.get('autoresponder_start'))
        self.assertEqual(case_type.autoresponder_due, expected_result.get('autoresponder_due'))
        self.assertEqual(case_type.autoresponder_message, expected_result.get('autoresponder_message'))

    def test_autoresponder_requires_date_and_message(self):
        case_type = CaseType.objects.create(autoresponder_message="Expected message",
                                            autoresponder_start=localtime())

        self.assertTrue(case_type.is_autoresponder_active)

    def test_autoresponder_disabled_when_start_date_is_missing(self):
        case_type = CaseType.objects.create(autoresponder_message="Expected message")

        self.assertFalse(case_type.is_autoresponder_active)

    def test_autoresponder_disabled_when_message_is_missing(self):
        case_type = CaseType.objects.create(autoresponder_start=localtime())

        self.assertFalse(case_type.is_autoresponder_active)

    def test_autoresponder_disabled_after_due_date(self):
        case_type = CaseType.objects.create(autoresponder_message="Expected message",
                                            autoresponder_start=localtime() - datetime.timedelta(days=10),
                                            autoresponder_due=localtime() - datetime.timedelta(days=1))

        self.assertFalse(case_type.is_autoresponder_active)

    def test_autoresponder_disabled_before_start_date(self):
        case_type = CaseType.objects.create(autoresponder_message="Expected message",
                                            autoresponder_start=localtime() + datetime.timedelta(days=10))

        self.assertFalse(case_type.is_autoresponder_active)
