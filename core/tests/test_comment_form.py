from unittest import mock

from django.test import TestCase
from faker import Factory

from core.factories import CaseFactory, CaseTypePhaseFactory
from pleio_auth.factories import UserFactory


class CaseFromTestCase(TestCase):

    def setUp(self):
        super(CaseFromTestCase, self).setUp()

        self.staff = UserFactory()
        self.case = CaseFactory(casetype__type='pleio')

        self.phase1 = CaseTypePhaseFactory(casetype=self.case.casetype)
        self.phase2 = CaseTypePhaseFactory(casetype=self.case.casetype)
        self.phase_n = CaseTypePhaseFactory(casetype=self.case.casetype)

    @mock.patch("core.casetypes.Pleio.base.PublishToPleio.on_close")
    def test_last_state_calls_finalize_method(self, mocked_pleio_on_close):

        random_comment = Factory().create().sentence()

        self.client.force_login(self.staff)
        self.client.post(f'/comments/add', {
            "case_id": self.case.id,
            "phase": self.phase_n.id,
            "description": random_comment,
            "closing_tasks": 0,
        })

        mocked_pleio_on_close.assert_called()
