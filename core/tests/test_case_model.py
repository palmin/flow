from contextlib import contextmanager
from unittest import mock

from django.db.models import signals
from django.test import TestCase
from django.utils.timezone import localtime

from core.api import CaseViewSet
from core.models import Case, CaseType


class CaseModelTestCase(TestCase):

    def setUp(self):
        self.casetype, created = CaseType.objects.get_or_create(name="Case with autoresponder",
                                                                type="pleio",
                                                                autoresponder_start=localtime(),
                                                                autoresponder_message="Expected message")

    @mock.patch('core.casetypes.Pleio.base.handle_autoresponder')
    def test_save_calls_autorespond_once(self, mocked_handle_autoresponder):
        case, created = Case.objects.get_or_create(name="Test",
                                                   tags=[],
                                                   casetype=self.casetype)
        mocked_handle_autoresponder.assert_called_with(case)

        case.save()
        mocked_handle_autoresponder.assert_called_once()

    @mock.patch('core.casetypes.Pleio.base.handle_autoresponder')
    def test_save_calls_autorespond_if_applicable(self, mocked_handle_autoresponder):
        self.casetype.autoresponder_due = localtime()
        Case.objects.get_or_create(name="Test",
                                   tags=[],
                                   casetype=self.casetype)

        mocked_handle_autoresponder.assert_not_called()

    @mock.patch('core.casetypes.Pleio.base.handle_autoresponder')
    def test_perform_create_on_drf_modelviewset_handles_the_autoresponder(self, mocked_handle_autoresponder):
        with suppress_pre_save_signal():
            case, created = Case.objects.get_or_create(name="Test",
                                                       tags=[],
                                                       casetype=self.casetype)
        serializer = mock.Mock()
        serializer.save.return_value = case

        viewset = CaseViewSet()
        viewset.perform_create(serializer)

        mocked_handle_autoresponder.assert_called_with(case)


@contextmanager
def suppress_pre_save_signal():
    receivers = signals.pre_save.receivers
    try:
        signals.pre_save.receivers = []
        yield
    finally:
        signals.pre_save.receivers = receivers

