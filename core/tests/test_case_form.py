from django.test import TestCase
from django.utils import timezone

from core.factories import CaseFactory
from pleio_auth.factories import UserFactory


class CaseFromTestCase(TestCase):
    maxDiff = None

    def setUp(self):
        super(CaseFromTestCase, self).setUp()

        self.staff = UserFactory()
        self.case = CaseFactory()

    def case_summary(self, case):
        return [
            case.casetype.id,
            case.name,
            case.description,
            str(case.deadline),
            str(case.tags),
        ]

    def test_update_no_change(self):
        expect = self.case_summary(self.case)

        self.client.force_login(self.staff)
        response = self.client.post(f'/cases/edit/{self.case.id}', {
            'casetype': self.case.casetype.id,
            'name': self.case.name,
            'description': self.case.description,
            'deadline': str(self.case.deadline) if self.case.deadline else "",
            'tags': ', '.join(self.case.tags),
        })
        self.case.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")
        self.assertEqual(expect, self.case_summary(self.case))

    def test_update_deadline(self):
        FMT = "%Y-%m-%d"
        self.client.force_login(self.staff)

        expected_deadline = timezone.now() + timezone.timedelta(days=200)

        response = self.client.post(f'/cases/edit/{self.case.id}', {
            'casetype': self.case.casetype.id,
            'name': self.case.name,
            'description': self.case.description,
            'deadline': str(expected_deadline),
            'tags': ', '.join(self.case.tags),
        })
        self.case.refresh_from_db()
        actual_deadline = self.case.deadline

        self.assertEqual(actual_deadline.strftime(FMT),
                         expected_deadline.strftime(FMT))
