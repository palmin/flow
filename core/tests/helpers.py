import os

from django.test import TestCase


class FileTestCase(TestCase):
    def assertFileSizeGt(self, path, min_bytes, msg=None):
        self.assertTrue(os.path.getsize(path) > min_bytes, msg=msg or "File is unexpectedly not bigger then %s bytes" % min_bytes)

    def assertFileExists(self, path, msg=None):
        self.assertTrue(os.path.exists(path), msg=msg or "Unexpectedly did not find %s" % path)

    def assertIsFile(self, path, msg=None):
        self.assertTrue(os.path.isfile(path), msg=msg or "Unexpectedly found non-file %s" % path)

    def assertIsDir(self, path, msg=None):
        self.assertTrue(os.path.isdir(path), msg=msg or "Unexpectedly found non-directory %s" % path)
