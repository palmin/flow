from __future__ import unicode_literals

import json
import os
import uuid

from django.contrib.postgres.aggregates import StringAgg
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.functions import Cast
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.utils.timezone import localtime
from solo.models import SingletonModel
from django import forms
from core.lib import CaseTypeManager
from pleio_auth.models import User

manager = CaseTypeManager()


def _serialize(nk):
    return json.dumps(nk)


def _materialize(data):
    return json.loads(data)

class MultipleArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "widget": forms.CheckboxSelectMultiple,
            **kwargs
        }
        return super(ArrayField, self).formfield(**defaults)

class Setting(SingletonModel):
    def default_show_columns():
        return ["id","name","status","created_on","latest_phase_change","casetype","current_phase","deadline","responsible","tags"]
    COLUMN_NAMES = (
        ("id", "ID"),
        ("name", "Naam"),
        ("status", "Status"),
        ("created_on", "Startdatum"),
        ("latest_phase_change", "Looptijd"),
        ("casetype","Type"),
        ("current_phase", "Fase"),
        ("deadline", "Deadline"),
        ("responsible", "Verantwoordelijke"),
        ("tags", "Tags")
    )
    shown_columns = MultipleArrayField(
        models.CharField(
            choices=COLUMN_NAMES,
            max_length=100,
            blank=False,
           null=True
        ),
        default=default_show_columns,
        blank=False,
        null=False
    )
    
    def __unicode__(self):
        return u"Settings"

    class Meta:
        verbose_name = "Settings"

class Subscription(models.Model):
    user = models.ForeignKey(
        User, related_name='subscriptions', on_delete=models.CASCADE)
    phase = models.ForeignKey('CaseTypePhase', on_delete=models.CASCADE)


class CaseTypeManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class CaseType(models.Model):
    objects = CaseTypeManager()

    name = models.CharField(max_length=100, unique=True)
    type = models.CharField(
        max_length=100, choices=manager.options(), default='simple')
    settings = models.JSONField(default=dict, blank=True)

    autoresponder_start = models.DateTimeField(null=True, blank=True,
                                               verbose_name="Start op")
    autoresponder_due = models.DateTimeField(null=True, blank=True,
                                             verbose_name="Vervalt na")
    autoresponder_message = models.TextField(null=True, blank=True,
                                             verbose_name="Berichttekst")

    def natural_key(self):
        return (self.name,)

    @property
    def is_autoresponder_active(self):
        if self.autoresponder_message is None:
            return False
        if self.autoresponder_start is None or self.autoresponder_start >= localtime():
            return False
        if self.autoresponder_due is not None and self.autoresponder_due <= localtime():
            return False
        return True

    def __str__(self):
        return self.name

    def manager(self):
        return manager.casetypes.get(self.type, None)


class CaseTypePhaseManager(models.Manager):
    def get_by_natural_key(self, casetype, name):
        return self.get(casetype__name=casetype, name=name)


class CaseTypePhase(models.Model):
    class Meta:
        ordering = ['casetype', 'order', 'id']
        unique_together = [["casetype", "name"]]

    objects = CaseTypePhaseManager()

    casetype = models.ForeignKey(
        'CaseType', related_name='phases', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    order = models.PositiveIntegerField()

    def __str__(self):
        return "{} - {}".format(self.casetype.name, self.name)

    def natural_key(self):
        return (self.casetype.name, self.name)

    natural_key.dependencies = ("core.CaseType",)


class UIDNaturalKeyManager(models.Manager):
    def get_by_natural_key(self, natural_key):
        return self.get(uid=natural_key)


class CaseLog(models.Model):
    class Meta:
        ordering = ['-created_on']

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    case = models.ForeignKey(
        'Case', related_name='logs', on_delete=models.CASCADE)
    from_phase = models.ForeignKey(
        'CaseTypePhase', null=True, blank=True, on_delete=models.CASCADE)
    performed_by = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.CharField(max_length=100)
    to = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return (str(self.uid),)

class TextStringAgg(StringAgg):
    output_field = models.TextField()


class CaseManager(UIDNaturalKeyManager):
    def with_documents(self):
        vector=SearchVector('name', weight='A') + \
        SearchVector('description', weight='B')  + \
        SearchVector('external_author', weight='B')  + \
        SearchVector(TextStringAgg('comments__description', delimiter=' '), weight='C') + \
        SearchVector(TextStringAgg('comments__author__name', delimiter=' '), weight='C') + \
        SearchVector(TextStringAgg('external_comments__description', delimiter=' '), weight='C') + \
        SearchVector(TextStringAgg('external_comments__author', delimiter=' '), weight='C')        
        return self.get_queryset().annotate(document=vector)

class Case(models.Model):
    OPEN = 'OPEN'
    CLOSED = 'CLOSED'
    PENDING = 'PENDING'

    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (CLOSED, 'Gesloten'),
        (PENDING, 'In afwachting')
    )

    class Meta:
        ordering = ['-created_on']
        indexes = (GinIndex(fields=["search_vector"]),)

    objects = CaseManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    casetype = models.ForeignKey('CaseType', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    description = models.TextField(null=True)
    tags = ArrayField(models.CharField(max_length=100), blank=True)
    data = models.JSONField(default=dict)
    manager = models.ForeignKey(
        User, null=True, related_name='managing', on_delete=models.CASCADE)
    responsible = models.ForeignKey(
        User, null=True, related_name='responsible_for', on_delete=models.CASCADE)
    current_phase = models.ForeignKey(
        'CaseTypePhase', null=True, blank=True, on_delete=models.CASCADE)
    external_id = models.CharField(max_length=100, null=True)
    external_author = models.CharField(max_length=100, default="")
    
    is_open = models.BooleanField(default=True)
    status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, default=OPEN)
    deadline = models.DateTimeField(null=True, blank=True)
    latest_phase_change = models.DateTimeField(auto_now_add=True, null=True)
    attachments = GenericRelation('Attachment')
    created_on = models.DateTimeField(auto_now_add=True)

    search_vector = SearchVectorField(null=True) 

    def __str__(self):
        return self.name

    def natural_key(self):
        return (str(self.uid),)

    def status_verbose(self):
        return dict(Case.STATUS_CHOICES)[self.status]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if 'update_fields' not in kwargs or 'search_vector' not in kwargs['update_fields']:
            instance = self._meta.default_manager.with_documents().get(pk=self.pk)
            instance.search_vector = instance.document
            instance.save(update_fields=['search_vector'])

    def change_phase(self, new_phase, user):
        casetype_phases = list(self.casetype.phases.all())

        if new_phase not in casetype_phases:
            return False

        if new_phase == self.current_phase:
            return True

        if casetype_phases[-1] == new_phase:
            self.status = Case.CLOSED
            self.latest_phase_change = None
        else:
            self.status = Case.OPEN
            self.latest_phase_change = timezone.now()

        current_phase = self.current_phase
        self.current_phase = new_phase
        self.manager = None

        self.save()

        self.logs.create(performed_by=user, from_phase=current_phase,
                         event='changed_phase', to=new_phase.name)
        return True

    def claim(self, user):
        if self.manager:
            return False

        self.manager = user
        self.save()
        self.logs.create(performed_by=user, event='claimed')
        return True

    def unclaim(self, user):
        if not self.manager:
            return False

        self.manager = None
        self.save()
        self.logs.create(performed_by=user, event='unclaimed')
        return True

    def mark_closed(self, user):
        if self.status == Case.PENDING:
            self.status = 'CLOSED'
            self.save()
            self.logs.create(performed_by=user, event='mark_closed')
            return True
        else:
            return False

    def remove_responsible(self, user):
        if not self.responsible:
            return False

        self.responsible = None
        self.save()
        self.logs.create(performed_by=user, event='removed_responsible')
        return True


@receiver(pre_save, sender=Case)
def pre_save_case(sender, instance, **kwargs):
    if not instance.casetype.manager():
        return

    if instance.id is None and instance.casetype.is_autoresponder_active:
        instance.casetype.manager().handle_autoresponder(instance)


class Comment(models.Model):
    class Meta:
        ordering = ['created_on']

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    case = models.ForeignKey(
        'Case', related_name='comments', on_delete=models.CASCADE)
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    attachments = GenericRelation('Attachment')
    external_id = models.CharField(max_length=100, null=True)

    def natural_key(self):
        return (str(self.uid),)

    def can_edit(self, user):
        if user == self.author:
            return True

        if user.is_admin:
            return True

        return False


class ExternalComment(models.Model):
    class Meta:
        ordering = ['created_on']

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    author = models.TextField()
    case = models.ForeignKey(
        'Case', related_name='external_comments', on_delete=models.CASCADE)
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return (str(self.uid),)

    def can_edit(self, user):
        if user.is_admin:
            return True

        return False


class Attachment(models.Model):
    objects = UIDNaturalKeyManager()

    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey('content_type', 'object_id')
    file = models.FileField(upload_to='attachments/%Y/%m/%d/')
    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    def natural_key(self):
        return (str(self.uid),)

    def filename(self):
        return os.path.basename(self.file.name)


@receiver(post_save, sender=Comment)
@receiver(post_delete, sender=Comment)
def comment_changed(sender, instance, **kwargs):
    instance.case.save()


@receiver(post_save, sender=ExternalComment)
@receiver(post_delete, sender=ExternalComment)
def external_comment_changed(sender, instance, **kwargs):
    instance.case.save()
