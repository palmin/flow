# Generated by Django 2.1 on 2018-09-07 11:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20180907_1256'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExternalComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.TextField()),
                ('description', models.TextField(null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('case', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='external_comments', to='core.Case')),
            ],
            options={
                'ordering': ['created_on'],
            },
        ),
        migrations.RemoveField(
            model_name='caselog',
            name='description',
        ),
    ]
