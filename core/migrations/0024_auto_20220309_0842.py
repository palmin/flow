# Generated by Django 2.2.10 on 2022-03-09 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_auto_20220308_1723'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='casetype',
            name='autoresponder_text',
        ),
        migrations.AddField(
            model_name='casetype',
            name='autoresponder_message',
            field=models.TextField(blank=True, null=True, verbose_name='Message'),
        ),
        migrations.AlterField(
            model_name='casetype',
            name='autoresponder_due',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Due date'),
        ),
        migrations.AlterField(
            model_name='casetype',
            name='autoresponder_start',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Start date'),
        ),
    ]
