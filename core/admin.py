import csv

from django.contrib import admin
from django.http import StreamingHttpResponse
from django.urls import reverse
from solo.admin import SingletonModelAdmin
from .models import CaseType, CaseTypePhase, Case, Subscription, Setting


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def export_cases(casetypemodel, request, queryset):
    headers = ['name', 'url', 'users', 'current_phase', 'passed_phases', 'current_phase_id', 'passed_phase_ids']
    cases = [headers]

    for case in queryset:
        users = []
        phases = []
        phase_ids = []
        current_phase_name = ''
        current_phase_id = '0'

        for log in case.logs.all():
            users.append(log.performed_by.name)
            if log.from_phase:
                phases.append(log.from_phase.name)
                phase_ids.append(str(log.from_phase.id))
        users = ', '.join(list(set(users)))
        phases = ', '.join(list(set(phases)))
        phase_ids = ', '.join(list(set(phase_ids)))

        if case.current_phase:
            current_phase_name = case.current_phase.name
            current_phase_id = case.current_phase.id
        url = request.build_absolute_uri(reverse('view_case', kwargs={'id': case.id}))

        cases.append([case.name, url, users, current_phase_name, phases, current_phase_id, phase_ids])

    # return csv
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer, delimiter=';')
    response = StreamingHttpResponse((writer.writerow(row) for row in cases),
                                     content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="cases.csv"'
    return response


export_cases.short_description = "Exporteer geselecteerde cases"


class CaseTypeAdmin(admin.ModelAdmin):
    fieldsets = (
        ("Technische eigenschappen", {
            'fields': ("name", "type", "settings")
        }),
        ("Automatisch antwoord", {
            'description': "De autoresponder wordt ingeschakeld als zowel een start datum/tijd als een bericht zijn ingesteld. De autoresponder stopt na de vervaldatum.",
            'fields': ("autoresponder_start",
                       "autoresponder_due",
                       "autoresponder_message"),

        })
    )


class CaseTypePhaseAdmin(admin.ModelAdmin):
    list_display = ('casetype', 'name', 'order')


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('user', 'phase')

class SettingAdmin(SingletonModelAdmin):
    list_display = ('shown_columns',)


class CaseAdmin(admin.ModelAdmin):
    list_display = ('name',)
    actions = [export_cases]


admin.site.register(CaseTypePhase, CaseTypePhaseAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Setting, SettingAdmin)

admin.site.register(CaseType, CaseTypeAdmin)
admin.site.register(Case, CaseAdmin)

"""
titel: case.name
url: request.build_absolute_uri(reverse('view_case', kwargs={'id': case.id}))
fases: case.casetype.phases
users case.logs.performed_by
"""
