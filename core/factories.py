import random
from random import randrange

import factory
from django.core.files.base import ContentFile
from django.utils import timezone
from django.utils.crypto import get_random_string
from factory.fuzzy import FuzzyChoice

from core.models import Case, CaseType, CaseTypePhase, CaseLog, Comment, ExternalComment, Attachment
from pleio_auth.factories import UserFactory


class CaseTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CaseType
        django_get_or_create = ('name',)

    name = factory.Faker('word')


def multiple_sentences():
    from faker import Factory
    f = Factory.create()
    return "\n".join([
        f.sentence(),
        f.sentence(),
        f.sentence(),
        f.sentence(),
        f.sentence(),
    ])


def random_future_datetime(_):
    return timezone.now() + timezone.timedelta(days=randrange(21, 100))


def case_external_id_attribute(case):
    if case.casetype.type == 'pleio':
        return randrange(1000, 100000)
    return None


def case_phase_attribute(case):
    if CaseTypePhase.objects.filter(casetype=case.casetype).count() == 0:
        CaseTypePhaseFactory(casetype=case.casetype)
        CaseTypePhaseFactory(casetype=case.casetype)
        CaseTypePhaseFactory(casetype=case.casetype)
    return FuzzyChoice(CaseTypePhase.objects.filter(casetype=case.casetype)).fuzz()


class CaseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Case

    name = factory.Faker('sentence')
    casetype = factory.SubFactory(CaseTypeFactory)
    description = factory.LazyFunction(multiple_sentences)
    tags = factory.List([
        factory.Faker('word'),
        factory.Faker('word'),
        factory.Faker('word'),
    ])
    manager = factory.SubFactory(UserFactory)
    responsible = factory.SubFactory(UserFactory)
    current_phase = factory.LazyAttribute(case_phase_attribute)
    external_id = factory.LazyAttribute(case_external_id_attribute)
    status = factory.fuzzy.FuzzyChoice([c[0] for c in Case.STATUS_CHOICES])
    deadline = factory.LazyAttribute(random_future_datetime)


def next_phase_index(phase):
    return CaseTypePhase.objects.filter(casetype=phase.casetype).count()


class CaseTypePhaseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CaseTypePhase
        django_get_or_create = ('casetype', 'order')

    casetype = factory.SubFactory(CaseTypeFactory)
    name = factory.Faker('sentence')
    order = factory.LazyAttribute(next_phase_index)


class CommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Comment

    author = factory.SubFactory(UserFactory)
    case = factory.SubFactory(CaseFactory)
    description = factory.Faker('sentence')


class ExternalCommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExternalComment

    author = factory.Faker('name')
    case = factory.SubFactory(CaseFactory)
    description = factory.Faker('sentence')


class CaseLogFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CaseLog

    case = factory.SubFactory(CaseFactory)
    performed_by = factory.SubFactory(UserFactory)
    event = factory.Faker('word')
    to = factory.Faker('word')


def random_content_file(*args):
    return ContentFile(get_random_string(), "%s.txt" % get_random_string(10))


def random_content_object(*args):
    if 3 * random.random() > 2:
        return CommentFactory()
    return CaseFactory()


class AttachmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Attachment

    content_object = factory.LazyAttribute(random_content_object)
    file = factory.LazyAttribute(random_content_file)
