import logging

from core.casetypes.Pleio.connector import PleioConnector
from core.lib import BaseCaseTypeWrapper, CloseTaskBase

logger = logging.getLogger(__name__)


class PublishToPleio(CloseTaskBase):
    label = 'Publiceer naar de voorkant'

    @classmethod
    def on_close(cls, case, comment, request):
        try:
            if not case.external_id:
                logger.warning('Could not log case {} as it does not have an external_id'.format(case.id))
                return

            connector = PleioConnector(case)
            connector.send_feedback(comment.description)

            comment.external_id = connector.response['comment_id']
            comment.save()
        except Exception as e:
            logger.warning(msg="@publish_frontend: {}".format(str(e)))


def handle_autoresponder(case):
    try:
        connector = PleioConnector(case)
        connector.send_feedback(case.casetype.autoresponder_message)
    except Exception as e:
        logger.warning(msg="@handle_autoresponder: {}".format(str(e)))


class CaseTypeWrapper(BaseCaseTypeWrapper):
    name = 'pleio'

    closing_tasks = [
        PublishToPleio
    ]

    def handle_autoresponder(self, case):
        handle_autoresponder(case)

    def __init__(self, key):
        self.key = key
