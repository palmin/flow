import logging
import requests

from urllib.parse import urljoin

logger = logging.getLogger(__name__)


class PleioConnector:

    def __init__(self, case):
        self.case = case
        self.settings = case.casetype.settings
        self.message = ''
        self.response = None

    def assert_configured(self):
        if not self.settings.get('pleio_url'):
            raise self.NotConfigured()

        if not self.settings.get('pleio_access_token'):
            raise self.NotConfigured()

    def get_url(self):
        return urljoin(self.settings['pleio_url'], 'flow/comments/add')

    def get_headers(self):
        return {
            'Authorization': 'Bearer {}'.format(self.settings['pleio_access_token'])
        }

    def get_data(self):
        return {
            'container_guid': self.case.external_id,
            'description': self.message
        }

    def send_feedback(self, message):
        try:
            self.assert_configured()
            self.message = message

            self.response = None
            request_response = requests.post(url=self.get_url(),
                                             data=self.get_data(),
                                             headers=self.get_headers(), timeout=2)

            self.response = request_response.json()
            return True

        except self.NotConfigured:
            return False

    class NotConfigured(Exception):
        pass
