from django.contrib import admin
from .models import User, AccessRequest
from django.db import IntegrityError
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from email.utils import formataddr

def accept(modeladmin, request, queryset):
    for access_request in queryset:
        try:
            User.objects.create_user(
                email = access_request.email,
                name = access_request.name,
                password = None,
                external_id = access_request.external_id
            )

            send_mail(
                render_to_string('emails/access_granted_subject.txt', { 'access_request': access_request, 'host': request.get_host() }),
                render_to_string('emails/access_granted.txt', { 'access_request': access_request, 'host': request.get_host(), 'url': request.build_absolute_uri('/') }),
                settings.EMAIL_FROM,
                [formataddr((access_request.name, access_request.email))],
                fail_silently=True
            )

        except IntegrityError:
            pass

        access_request.delete()

accept.short_description = "Accept this access request"

class AccessRequestAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    readonly_fields = ('created_at', 'name', 'email', 'external_id')
    list_display = ('name', 'email', 'created_at')
    actions = [accept]

class UserAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
    
    readonly_fields = ('external_id', 'last_login')
    list_display = ('name', 'email', 'is_active', 'last_login')
    exclude = ('password', )

admin.site.register(User, UserAdmin)

if settings.PLEIO_OAUTH_REQUEST_ACCESS:
    admin.site.register(AccessRequest, AccessRequestAdmin)