import factory
from pleio_auth.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    name = factory.Faker('name')
    password = 'secret'
    email = factory.Faker('email')
    is_active = True
    is_admin = False
    external_id = factory.Faker('pyint', min_value=300, max_value=3000)