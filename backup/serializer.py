import json

from django.apps import apps
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Model


class FieldSerializerBase():
    field: models.Field = None
    is_autofield = False

    def __init__(self, field):
        self.field = field

    def applies(self):
        return False

    def auto_exclude(self):
        return []

    def serialize(self, object):
        raise NotImplementedError()

    def materialize(self, object):
        raise NotImplementedError()


class CommonFieldSerializer(FieldSerializerBase):
    def serialize(self, object):
        return getattr(object, self.field.name)

    def materialize(self, data):
        return data[self.field.name]


class FKFieldSerializer(FieldSerializerBase):
    def applies(self):
        return isinstance(self.field, models.ForeignKey)

    def serialize(self, object):
        try:
            foreign_item: Model = getattr(object, self.field.name)
            return json.dumps({
                "natural_key": foreign_item.natural_key(),
                "model": foreign_item._meta.label,
            })
        except AttributeError:
            pass

    def materialize(self, data):
        if not data.get(self.field.name):
            return None
        reference = json.loads(data[self.field.name])
        model = apps.get_model(reference['model'])
        return model.objects.get_by_natural_key(*reference['natural_key'])


class StrFieldSerializer(FieldSerializerBase):
    def applies(self):
        return isinstance(self.field, models.UUIDField)

    def serialize(self, object):
        return self._str(getattr(object, self.field.name))

    def _str(self, value):
        if not value:
            return ''
        return str(value)

    def materialize(self, data):
        return data[self.field.name]


class DateTimeFieldSerializer(StrFieldSerializer):
    def __init__(self, field):
        super().__init__(field)
        try:
            self.is_autofield = field.auto_now_add or field.auto_now
        except AttributeError:
            pass

    def applies(self):
        return isinstance(self.field, (
            models.DateField,
            models.DateTimeField))

    def materialize(self, data):
        return data[self.field.name] or None

class JsonFieldSerializer(FieldSerializerBase):
    def applies(self):
        return isinstance(self.field, (
            models.JSONField,
            ArrayField))

    def serialize(self, object):
        return json.dumps(getattr(object, self.field.name))

    def materialize(self, data):
        return json.loads(data[self.field.name])


class GenericFKFieldSerializer(FKFieldSerializer):
    field: GenericForeignKey

    def applies(self):
        return isinstance(self.field, (GenericForeignKey,))

    def auto_exclude(self):
        return [self.field.fk_field, self.field.ct_field]


class GenericRelationFieldSerializer(FieldSerializerBase):
    def applies(self):
        return isinstance(self.field, GenericRelation)

    def auto_exclude(self):
        yield self.field.name


class BackupSerializerBase():
    model = None
    exclude = []
    modifier = {}

    def __init__(self):
        assert self.model, "Specify the model"
        self.field_map = {field.name: field for field in self.model._meta.fields}
        self.field_map.update({field.name: field for field in self.model._meta.private_fields})
        for field in self.field_map.values():
            try:
                serializer = self._get_serializer(field)
                for field in serializer.auto_exclude():
                    self.exclude.append(field)
            except Exception:
                pass

    def serialize(self, queryset):
        for object in queryset.all():
            yield self.serialize_item(object)

    def _get_serializer(self, field):
        for serializer in [FKFieldSerializer(field),
                           StrFieldSerializer(field),
                           DateTimeFieldSerializer(field),
                           JsonFieldSerializer(field),
                           GenericRelationFieldSerializer(field),
                           GenericFKFieldSerializer(field)]:
            if serializer.applies():
                return serializer
        return CommonFieldSerializer(field)

    def serialize_item(self, item):
        data = {"natural_key": json.dumps(item.natural_key())}

        for field_name, field in self.field_map.items():
            if field_name in self.exclude:
                continue
            if getattr(field, 'primary_key', None):
                continue

            field_serializer = self._get_serializer(field)

            assert field_serializer, "No instance found for %s.%s" % (self.model, field_name)
            data[field_name] = field_serializer.serialize(item)

        return data

    def materialize_item(self, data):
        assert self.model, "Specify the model for this serializer"

        natural_key = json.loads(data['natural_key'])
        try:
            recovered_item: Model = self.model.objects.get_by_natural_key(*natural_key)
        except self.model.DoesNotExist:
            recovered_item: Model = self.model()

        autofields = {}
        for field_name, value in data.items():
            if field_name == 'natural_key':
                continue
            field = self.field_map[field_name]
            instance = self._get_serializer(field)
            assert instance, "No instance found for %s.%s" % (self.model, field_name)
            if instance.is_autofield:
                autofields[field_name] = instance.materialize(data)
            setattr(recovered_item, field_name, instance.materialize(data))

        recovered_item.save()

        if autofields:
            self.model.objects.filter(pk=recovered_item.pk).update(**autofields)
        recovered_item.refresh_from_db()

        return recovered_item
