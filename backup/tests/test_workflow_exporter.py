import os.path
import shutil
from typing import Union

from backup.io.serialize import WorkflowExporter
from core.factories import (CaseTypePhaseFactory, CaseFactory,
                            CaseLogFactory, CommentFactory, ExternalCommentFactory, AttachmentFactory)
from core.tests.helpers import FileTestCase
from pleio_auth.factories import UserFactory


class TestWorkflowExporterTestCase(FileTestCase):
    serializer: Union[WorkflowExporter, None] = None
    trash_can = []

    def setUp(self):
        super().setUp()

        CaseTypePhaseFactory(casetype__name='test', order=1)
        CaseTypePhaseFactory(casetype__name='test', order=2)
        CaseTypePhaseFactory(casetype__name='test', order=3)

    def build_attachment(self, **kwargs):
        attachment = AttachmentFactory(**kwargs)
        self.trash_can.append(attachment.file.path)
        return attachment

    def tearDown(self):
        if self.serializer:
            self.cleanup(self.serializer.target_path)
            self.cleanup(self.serializer.archive_path)
        for path in self.trash_can:
            self.cleanup(path)
        super().tearDown()

    def cleanup(self, path):
        try:
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.unlink(path)
        except Exception:
            pass

    def assertBackupCreated(self, path):
        self.assertIsNotNone(path)
        self.assertTrue(os.path.exists(path))
        self.assertTrue(os.path.isdir(path))

    def test_serialize_commons(self):
        self.serializer = WorkflowExporter()
        self.serializer.run()
        self.assertBackupCreated(self.serializer.target_path)

    def test_export_cases(self):
        # given
        CaseFactory()

        # when
        self.serializer = WorkflowExporter()
        self.serializer.run()

        # then
        self.assertBackupCreated(self.serializer.target_path)
        self.assertFileExists(os.path.join(self.serializer.target_path, 'cases.csv'))
        self.assertFileSizeGt(os.path.join(self.serializer.target_path, 'cases.csv'), 0)

    def test_export_caselogs(self):
        # Given
        CaseLogFactory()

        # When
        self.serializer = WorkflowExporter()
        self.serializer.run()

        # Then
        self.assertBackupCreated(self.serializer.target_path)
        self.assertFileExists(os.path.join(self.serializer.target_path, 'caselogs.csv'))
        self.assertFileSizeGt(os.path.join(self.serializer.target_path, 'caselogs.csv'), 0)

    def test_export_comments(self):
        # Given
        comment = CommentFactory()
        self.build_attachment(content_object=comment)

        # When
        self.serializer = WorkflowExporter()
        self.serializer.run()

        # Then
        self.assertBackupCreated(self.serializer.target_path)
        self.assertFileExists(os.path.join(self.serializer.target_path, 'comments.csv'))
        self.assertFileSizeGt(os.path.join(self.serializer.target_path, 'comments.csv'), 0)

    def test_export_external_comments(self):
        # Given
        ExternalCommentFactory()

        # When
        self.serializer = WorkflowExporter()
        self.serializer.run()

        # Then
        self.assertBackupCreated(self.serializer.target_path)
        self.assertFileExists(os.path.join(self.serializer.target_path, 'externalcomments.csv'))
        self.assertFileSizeGt(os.path.join(self.serializer.target_path, 'externalcomments.csv'), 0)

    def test_export_attachments(self):
        # Given
        case = CaseFactory()
        self.build_attachment(content_object=case)

        # When
        self.serializer = WorkflowExporter()
        self.serializer.run()

        # Then
        self.assertBackupCreated(self.serializer.target_path)
        self.assertFileExists(os.path.join(self.serializer.target_path, 'attachments.csv'))
        self.assertFileSizeGt(os.path.join(self.serializer.target_path, 'attachments.csv'), 0)
