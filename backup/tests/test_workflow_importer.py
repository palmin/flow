import os

from django.db.models import Model
from django.test import TestCase

from backup.serializer import BackupSerializerBase
from core.backup_serializers import (CaseBackupSerializer, CaseLogBackupSerializer, CommentBackupSerializer,
                                     ExternalCommentBackupSerializer, AttachmentBackupSerializer)
from core.factories import (CaseFactory, CaseLogFactory, CommentFactory, ExternalCommentFactory,
                            AttachmentFactory)


class Template:
    class TestWorkflowImporterTestCaseBase(TestCase):
        maxDiff = None

        def setUp(self):
            self.serializer = self.create_serializer()

            item = self.create_item()
            self.record = self.serializer.serialize_item(item)
            item.delete()

        def create_item(self) -> Model:
            raise NotImplementedError()

        def create_serializer(self) -> BackupSerializerBase:
            raise NotImplementedError()

        def test_serialize_unserialize(self):
            reversed_item = self.serializer.materialize_item(self.record)
            reversed_record = self.serializer.serialize_item(reversed_item)

            self.assertDictEqual(self.record, reversed_record)


class TestWorkflowCaseImporterTestCase(Template.TestWorkflowImporterTestCaseBase):
    def create_item(self) -> Model:
        return CaseFactory()

    def create_serializer(self) -> BackupSerializerBase:
        return CaseBackupSerializer()


class TestWorkflowCaseLogImporterTestCase(Template.TestWorkflowImporterTestCaseBase):
    def create_item(self) -> Model:
        return CaseLogFactory()

    def create_serializer(self) -> BackupSerializerBase:
        return CaseLogBackupSerializer()


class TestWorkflowCommentImporterTestCase(Template.TestWorkflowImporterTestCaseBase):
    def create_item(self) -> Model:
        return CommentFactory()

    def create_serializer(self) -> BackupSerializerBase:
        return CommentBackupSerializer()


class TestWorkflowExternalCommentImporterTestCase(Template.TestWorkflowImporterTestCaseBase):
    def create_item(self) -> Model:
        return ExternalCommentFactory()

    def create_serializer(self) -> BackupSerializerBase:
        return ExternalCommentBackupSerializer()


class TestWorkflowAttachmentImporterTestCase(Template.TestWorkflowImporterTestCaseBase):
    cleanup = []

    def tearDown(self):
        for path in self.cleanup:
            os.unlink(path)
        super().tearDown()

    def create_item(self) -> Model:
        attachment = AttachmentFactory()
        self.cleanup.append(attachment.file.path)
        return attachment

    def create_serializer(self) -> BackupSerializerBase:
        return AttachmentBackupSerializer()
