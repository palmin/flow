"""flow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from rest_framework import routers
from core import views, api
import pleio_auth.views as auth_views

router = routers.DefaultRouter()
router.register(r'cases', api.CaseViewSet)
router.register(r'casetypes', api.CaseTypeViewSet)
router.register(r'caselogs', api.CaseLogViewSet)
router.register(r'externalcomments', api.ExternalCommentViewSet)

urlpatterns = [
    url(r'^$', views.cases, name='cases'),
    url(r'^notifications', views.notifications, name='notifications'),
    url(r'^cases/add', views.add_case, name='add_case'),
    url(r'^cases/view/(?P<id>[0-9]+)/$', views.view_case, name='view_case'),
    url(r'^cases/view/(?P<id>[0-9]+)/logs$', views.view_case_logs, name='view_case_logs'),
    url(r'^cases/claim/(?P<id>[0-9]+)$', views.claim_case, name='claim_case'),
    url(r'^cases/unclaim/(?P<id>[0-9]+)$', views.unclaim_case, name='unclaim_case'),
    url(r'^cases/mark_closed/(?P<id>[0-9]+)$', views.mark_closed, name='mark_closed'),
    url(r'^cases/remove_responsible/(?P<id>[0-9]+)$', views.remove_responsible, name='remove_responsible'),
    url(r'^cases/edit/(?P<id>[0-9]+)', views.edit_case, name='edit_case'),
    url(r'^cases/delete/(?P<id>[0-9]+)', views.delete_case, name='delete_case'),
    url(r'^comments/add', views.add_comment, name='add_comment'),
    url(r'^comments/edit/(?P<id>[0-9]+)', views.edit_comment, name='edit_comment'),
    url(r'^comments/delete/(?P<id>[0-9]+)', views.delete_comment, name='delete_comment'),
    url(r'^auth/', include('pleio_auth.urls')),
    url(r'^admin/login/', auth_views.login, name='admin_login'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls))
]

if settings.DEBUG:
     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)