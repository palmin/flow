import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DEBUG') == 'True'

ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST')]

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.getenv('DB_HOST'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'NAME': os.getenv('DB_NAME'),
    }
}

CASE_TYPES = {
    'simple': {
        'ENGINE': 'core.casetypes.Simple',
    },
    'pleio': {
        'ENGINE': 'core.casetypes.Pleio',
    }
}
if DEBUG:
    CASE_TYPES['message'] = {
        'ENGINE': 'core.casetypes.Message'
    }

STATIC_ROOT  = '/app/static'

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'nl-nl'

TIME_ZONE = 'Europe/Amsterdam'

PLEIO_OAUTH_URL = os.getenv('PLEIO_OAUTH_URL')
PLEIO_OAUTH_CLIENT_ID = os.getenv('PLEIO_OAUTH_CLIENT_ID')
PLEIO_OAUTH_CLIENT_SECRET = os.getenv('PLEIO_OAUTH_CLIENT_SECRET')
PLEIO_OAUTH_REQUEST_ACCESS = True

EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = os.getenv('EMAIL_PORT')
EMAIL_HOST_USER = os.getenv('EMAIL_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_PASSWORD')
EMAIL_USE_TLS = True if os.getenv('EMAIL_USE_TLS') in ['1', 'YES', 'TRUE'] else False

EMAIL_FROM = os.getenv('EMAIL_FROM')

DELETE_CASE_DISABLED = os.getenv('DELETE_CASE_DISABLED') == 'True'
